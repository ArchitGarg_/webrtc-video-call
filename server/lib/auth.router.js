const express = require('express'); 
const router = express.Router(); 
const passport = require('passport'); 
const authController = require('./auth.controller'); 

const scopes = ['profile', 
'https://www.googleapis.com/auth/gmail.settings.basic', 
'https://www.googleapis.com/auth/gmail.settings.sharing']

const googleAuth = passport.authenticate('google', { scope: scopes}); 

router.get('/google/callback', googleAuth, authController.google); 

router.use((req, res, next) => { 
    req.session.socketId = req.query.socketId; 
    next(); 
})

router.get('/google', googleAuth); 

module.exports = router; 
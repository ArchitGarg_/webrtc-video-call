const API_DEV_HOSTNAME = "http://localhost:5000/"; 

//we won't worry about prod just yet
const API_PROD_HOSTNAME = ""; 

const CLIENT_DEV_HOSTNAME = "http://localhost:8080/"; 

//we won't worry about prod just yet
const CLIENT_PROD_HOSTNAME = ""; 

export const getApiHostname = () => { 
    if (process.env.NODE_ENV === "production") 
        return API_PROD_HOSTNAME; 
    else return API_DEV_HOSTNAME; 
}

export const getClientHostname = () => { 
    if (process.env.NODE_ENV === "production")
        return CLIENT_PROD_HOSTNAME; 
    else return CLIENT_DEV_HOSTNAME; 
}
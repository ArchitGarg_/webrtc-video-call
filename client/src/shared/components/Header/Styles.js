//here we leverage styled-components to more cleanly create seperate html elements with their own css
import styled from 'styled-components'; 

export const Container = styled.div`
    width: 100%; 
    background-color: white; 
    height: 6em; 

    z-index: 1000; 

    display: flex; 
    justify-content: flex-start;
    align-items: center;
    align-content: center; 

    position: fixed; 

    box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04);
`; 
import React from 'react'; 

//imported as a styled-component
import { Container } from './Styles'; 

const Header = () => { 

    return (<Container>
        <img style={{marginLeft: '30px'}} alt="Image" src="https://ebssecureforms.com/wp-content/uploads/2020/03/EbsHealthcareLogoWebTransparent_200.png" width="100" height="56" />
    </Container>)
}

export default Header; 
import React from 'react'; 

import OAuth from './OAuth'; 

import io from 'socket.io-client'; 

import { getApiHostname } from 'shared/constants/apiconstants'; 

const socket = io(getApiHostname()); 

//here we define an array that maps to a series of OAuth login buttons, 
//it's unlikely we'll support anything other than Google but the option is there if we want it. 
const providers = ['google']; 

export default class LoginPage extends React.Component
{ 
    render() { 
        const buttons = (providers, socket) =>
            providers.map(provider =>  
                <OAuth
                    provider={provider}
                    key={provider}
                    socket={socket}
                    history={this.props.history}
                />
        )

        let b = buttons(providers, socket); 

        //This is the style I used for my last project with EBS, we haven't decided on a style just yet, I'd be ok with going with Semantic-UI but we want to ask other members 
        //of our team for input. 
        return (
            <div>
                <div style={{
                    position: 'absolute', 
                    width: '500px', 
                    height: '300px', 
                    top: '50%', 
                    left: '50%', 
                    margin: '-150px 0 0 -250px', 
                    backgroundColor: 'white', 
                    display: 'flex', 
                    justifyContent: 'flex-start', 
                    flexDirection: 'column', 
                    borderRadius: '.25em', 
                    fontFamily: 'system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji',
                    boxShadow: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)'
                }}>
                    <div style={{
                        width: '100%', 
                        height: '50px', 
                        backgroundColor: '#616E7C', 
                        borderRadius: '.25em', 
                        borderBottomLeftRadius: 0, 
                        borderBottomRightRadius: 0, 
                        textAlign: 'center'}}>
                        <h1 style={{color: 'white', fontSize: '16px'}}>EBS Teletherapy Login</h1>
                    </div>
                    <div style={{
                        display: 'flex', 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                        textAlign: 'center', 
                        height: '450px'
                    }}>
                    {b}
                    </div>
                </div>
            </div>
        )
    }
}
import React from 'react'; 
import { Router, Switch, Route, Redirect } from 'react-router-dom'; 

import Dashboard from 'Dashboard'; 
import Auth from 'Auth'; 

import history from 'browserHistory'; 

const Routes = () => {

    //redirects to login if we're trying to access a private route without a token
    function PrivateRoute({component: Component, ...rest}) { 
        let token = localStorage.getItem('token'); 
        let isAuthenticated = (token) ? true : false; 
        return (
            <Route 
                {...rest}
                render={props => 
                isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                      to={{
                          pathname: "/login", 
                          state: { from: props.location} 
                      }}
                    />
                )} 
            />
        )
    }

    return(
    <Router history={history}>
        <Switch>
            <Redirect exact from='/' to='/dashboard' /> 
            <PrivateRoute path="/dashboard" component={Dashboard} /> 
            <Route path="/login" component={Auth} /> 
        </Switch>
    </Router>)
}

export default Routes; 